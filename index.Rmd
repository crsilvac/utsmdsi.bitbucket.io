--- 
title: "MDSI Student Guide"
author: "Authored by, and for MDSI students"
cover-image: Images/FAlogo.png
url: 'https\://utsmdsi.bitbucket.io/'
site: bookdown::bookdown_site
output: bookdown::gitbook
documentclass: book
description: "This is a living document collaboratively authored by students in the Master of Data Science and Innovation course at UTS"
---

# Welcome {-}

<img src="Images/FAlogo.png" width="250" height="375" alt="Cover image" align="right" style="margin: 0 1em 0 1em" />

The purpose of this book is to ensure that you - presumably a new MDSI student - have a trusted source of information that will help you to rapidly onboard you into the wild ride that is MDSI. We hope that this book helps you get up to speed quickly and serves as a useful quick-reference guide during your first year in the program.

You may see this book referred to as **FlipAround** from time to time - this was the original name for the book, and was a play on **CICAround**, which was the blogging platform used when the MDSI program was part of the Connected Intelligence Centre at UTS. Since moving to the Faculty of Transdisciplinary Innovation (FDTI) in 2019 this guide has been renamed **MDSI Student Guide** to make it clear to new students without the memory of **CICAround**. As a tribute to the old name, we have kept the original **FlipAround** logo for this book.

## Welcome message

MDSI is unique in its approach and feel. MDSI is a ‘boutique degree’ which means we are a small tight-knit data family which means the contacts you walk out (really) knowing are going to be more valuable than the skills you learn. In terms of content, our point of difference is the innovation in our name. We take our innovation component as seriously as data science, and is ingrained in everything that’s taught. For us, a data science degree was our innovation (we were the first of its kind in Australia), and in the rapidly changing context that is data, the ability to innovate and adapt is a pretty great point of difference for you too.

Data science is a collaborative discipline. Students in the MDSI program get hands on experience of working in teams to formulate and solve real-life data science problems. Most courses focus on techniques to solve problems, but spend very little time (if any) on how problems should be formulated. The MDSI program is structured in a way that helps students learn this tacit, but crucial skill.

Another important aspect of data science is that it is a rapidly evolving field. A data scientist must therefore be able to stay current with developments in the field. The MDSI program, with its emphasis on critical self-learning, prepares students to be lifelong learners.

Welcome, and good luck on your MDSI journey.

## Checklist of things to do

Getting started on your MDSI journey can be somewhat overwhelming. So to help you ease into life as an MDSI student, the following checklist will help you to get up and running as painlessly as possible.

 - Do your statistics pre-flight test
 - Activate UTS student email
 - Forward UTS student email if required
 - Enrol in your subjects
 - Review Subject Outlines
 - Activate and personalise CICAround
 - Join the Slack Channel
 - Do your CLARA profile
 - Download R & R Studio
 - Download Tableau
 - Activate Github
 - Download Python & Rodeo (optional)
 - Download Rapidminer (optional)
 - Test your Google Drive
 - Test your Office 365 Drive
 - Download Quantum GIS (optional)
 - Download KNIME (optional)
 - Log into Diigo
 - Log into SPARK
 - Log into Review

  
## Pre-flight tests

MDSI statistics pre-flight test:
http://www.uts.edu.au/future-students/analytics-and-data-science/essential-information/mdsi-statistics-pre-flight-test

## MDSI Jargon
```{r echo=FALSE}
MDSIJargon = read.csv("MDSIJargon.csv")
knitr::kable(MDSIJargon)
```


